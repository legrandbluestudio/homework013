#include <iostream>
#include <iomanip>
#include <ctime>
#include <cmath>
using namespace std;

class Vector

{
private:
	
	int x;
	int y;
	int z;
	double s;
	
public:
	
	Vector(int valueX, int valueY, int valueZ)
	{
		x = valueX;
		y = valueY;
		z = valueZ;
		
		
		s = pow(valueX, 2) + pow(valueY, 2) + pow(valueZ, 2);
		 
		
	}
	

	void show()
	{
		cout << " " << x << " " << y << " " << z << endl;
		cout << " " << sqrt(s) << endl;
	}

};

int main()
{

	Vector a(3,3,3);
	
	

	a.show();
	
	

	return 0;
}